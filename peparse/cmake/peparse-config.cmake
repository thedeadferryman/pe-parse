set(_LNAME peparse)
set(_HDR_FILE peparse)

set(_PEPARSE_POSSIBLE_ROOTS
        "${PEPARSE_ROOT}"
        "./lib${_LNAME}"
        "../lib${_LNAME}"
        )

set(_PEPARSE_POSSIBLE_SUFS
        "lib"
        ""
        )

set(_PEPARSE_POSSIBLE_NAMES
        "lib${_LNAME}"
        "lib${_LNAME}.dll"
        "${_LNAME}"
        "${_LNAME}.dll"
        )


find_library(tc_LIBRARY
        NAMES ${_PEPARSE_POSSIBLE_NAMES}
        PATHS ${_PEPARSE_POSSIBLE_ROOTS}
        PATH_SUFFIXES ${_PEPARSE_POSSIBLE_SUFS}
        )

find_path(tc_INCLUDE_DIR
        NAMES tc.h
        PATHS ${_PEPARSE_POSSIBLE_ROOTS}
        PATH_SUFFIXES "include"
        )

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(peparse
        DEFAULT_MSG
        peparse_LIBRARY peparse_INCLUDE_DIR)

mark_as_advanced(peparse_INCLUDE_DIR peparse_LIBRARY)

if (peparse_FOUND AND NOT TARGET libpeparse)
    add_library(peparse UNKNOWN IMPORTED)
    set_target_properties(peparse PROPERTIES
            IMPORTED_LOCATION "${peparse_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${peparse_INCLUDE_DIR}")
endif ()

if (peparse_FOUND)
    message(STATUS "Found libpeparse")
    message(STATUS "  libpeparse: ${peparse_LIBRARY}")
    message(STATUS "  peparse.h: ${peparse_INCLUDE_DIR}/peparse.h")
endif ()
