cmake_minimum_required(VERSION 3.7)
project(petools)

set(CMAKE_CXX_STANDARD 14)

if (NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE "RelWithDebInfo")
endif ()

include(cmake/compilation_flags.cmake)
list(APPEND GLOBAL_CXXFLAGS ${DEFAULT_CXX_FLAGS})

option(BUILD_SHARED_LIBS "Build Shared Libraries" OFF)
option(BUILD_CLI_TOOLS "Build Command Line Tools" ON)


if (MSVC)
  set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON)
endif ()

add_subdirectory(peparse)

if (BUILD_CLI_TOOLS)
	add_subdirectory(examples/pedump)
	add_subdirectory(examples/peaddrconv)
endif ()

if (BUILD_SHARED_LIBS)
	set(BUILD_SHARED_LIBS_MESSAGE enabled)
else ()
	set(BUILD_SHARED_LIBS_MESSAGE disabled)
endif ()

if (BUILD_CLI_TOOLS)
	set(BUILD_CLI_TOOLS_MESSAGE enabled)
else ()
	set(BUILD_CLI_TOOLS_MESSAGE disabled)
endif ()

message(STATUS "Build Configuration: ${CMAKE_BUILD_TYPE}")
message(STATUS "Build Shared: ${BUILD_SHARED_LIBS_MESSAGE}")
message(STATUS "Build CLI Tools: ${BUILD_CLI_TOOLS_MESSAGE}")
message(STATUS "Install Prefix: ${CMAKE_INSTALL_PREFIX}")

install(DIRECTORY "examples"
		DESTINATION "share/peparse"
		)